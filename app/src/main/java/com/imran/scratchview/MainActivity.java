package com.imran.scratchview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ScratchView scratchView = findViewById(R.id.text);
        scratchView.setRevealListener(new ScratchView.IRevealListener() {
            @Override
            public void onRevealed(ScratchView tv) {

            }

            @Override
            public void onTouchEvent(MotionEvent event) {
                Log.d(TAG, "onTouch");
            }

            @Override
            public void onRevealPercentChangedListener(ScratchView stv, float percent) {

            }
        });
    }
}
